# Tor's Hackweek

This repository is being used to document and coordinate current hackweek at Tor. Old hackweek's documentation will be archived in 'archived' folder in the same repository.

## Next Hackweek: March 29th to April 2nd - 2021

When: Monday March 29th to Friday April 2nd - starts each day at 1500UTC.
Where: irc & BBB links will be available for the projects

This hackweek aims to promote small projects that can be finished in 5 days. We will have a list of projects proposed by people from the Tor community. We will do a call for help for people to join each of those projects. Each group will meet during the week and work on its specific project. At the end of the week (on Friday) we will have a DEMO day where each group will present their work.

Each project will list this information:
 - Project/team name
 - Pad for the team (the pad will have all this info + BBB link)
 - Main point of contact for the project/team (timezone you are in)
 - Summary of the work you want to do
 - Skills you need (for example, someone who knows onionperf, designer, translator,
code in rust - things like this)

# LOUNGE space

This BBB room will be open through the week. We will have the welcome/opening session here on Monday as well as the demo day on Friday.

ROOM: link to be posted on 14:45 UTC on Monday

## Monday Opening Session: 15 UTC

1. Logistics.
  . main link with information
  . information about lounge every day
  . information about demo day
2. Each group present their project.
3. Each group will go to their meeting place (BBB or irc or whatever else they may choose).
4. There will be a "lounge" space for people to drop in during the week in case there are questions or comments, etc.

## Friday DEMO Day

1. Each group demo their work.

# PROJECTS/GROUPS

To add yourself to a group use the group's pad please.

1 Torify signal-cli with torsocks
- Pad: https://pad.riseup.net/p/2021-hackweek-signal-cli
- Contact: jnewsome and/or dgoulet
- Summary: Work through blocking bugs for getting signal-cli working with torsocks. Interesting because signal is being blocked in some locales (Iran).
- Skills: C. Maybe familiarity with libc, LD_PRELOAD


2 Prometheus alerts for anti-censorship metrics
- Pad: https://pad.riseup.net/p/2021-hackweek-prometheus-alerts
- Contact: cohosh (maybe anarcat?)
- Summary: We have BridgeDB exporting prometheus metrics so far, and we could implement this for Snowflake. It would be great if we could get alerts when usage changes to notify us of possible censorship events. Somewhat related, it would also be nice to get alerts when default bridge usage drops off suddenly or directly connecting Tor users from different regions.
- Skills: maybe Go (for changes to snowflake), maybe Python for other services, some sysadmin experience to figure out how to do the alerts, metrics pipeline experience.

3 Prototype network-namespace-based torsocks
- Pad: https://pad.riseup.net/p/2021-hackweek-network-namespace
- Contact: jnewsome and/or dgoulet
- Summary: Use network namespaces (or maybe something else?) to run target software in an environment where it can't talk to the real network; it can only talk to the tor socks port and/or some "shim" adapter. Might be able to remove or lessen dependence on LD_PRELOAD (which isn't available everywhere, can be "escaped", and can be a bit fragile). If we continued to use LD_PRELOAD could at least be used to prevent accidental connections to the real network.
- Skills: C, familiarity with network namespaces and/or LD_PRELOAD would be helpful. New code could potentially be written in Rust.

4 Add support for UDP sockets over onion services, possibly with enabling support for WebRTC in Tor Browser
- Pad: https://pad.riseup.net/p/2021-hackweek-udp
- Contact: sysrqb (UTC-5(-ish))
- Skills: C/C++, familiar with tor and/or Firefox and/or webrtc
- Goal: Present Hack Week results via Tor Browser on BBB
- Intermediate Tor steps:
  - Add support for relaying UDP datagrams
  - Add support for configuring UDP onion services
- Intermediate Tor Browser steps:
  - Enable WebRTC
  - ?

5 Visualize Tor metrics's data in ways that it can be useful for community
- Pad: https://pad.riseup.net/p/2021-hackweek-metrics-viz
- Contact: gus and gaba (UTC-3)
- Skills: data visualization
- Summary: The goal is to have a dashboard with Tor usage per country in a way that is easy to see big changes happening. Right now we need to select each country to see the Tor usage. It would be good to have a way to see all the countries and the onews where usage is increasing (via bridge and direct connection)

6 Prototype Rust+Arti-based HTTP frontend cache for directory authorities
- Pad: https://pad.riseup.net/p/2021-hackweek-arti
- Contact: nickm (UTC-5)
- Skills: Rust hacking experience helpful.
- Summary: Directory authorities are under a lot of unnecessary load from excessive download requests.  We have other projects in mind to reduce those requests, but one workaround is to add a frontend cache in front of one or more of the authorities' HTTP ports.  With this project we'll write a Rust server use Arti's download and validation code to fetch directory information from an authority, and expose that information via a set of HTTP requests.  With luck, our code will be reuseable in the future when relays or authorities are rewritten in Rust.


7 Onionshare download accelerator
- Pad: https://pad.riseup.net/p/2021-hackweek-onionshare-accelerator
- Contact: Mike (UTC-6) or maybe Micah (UTC-8?)
- Skills: Python, HTTP/1.1, SOCKS
- Journalists are requesting ways to download large files from onion services faster. This is actually possible already with HTTP, without any Tor client modifications. By splitting up requests into 250k chunks using HTTP range requests, and using SOCKS username and password to allocate these requests properly onto different circuits, we can hit ~6 megabytes/sec per guard (so ~12 megabytes/sec with 2 gaurds), with a custom HTTP download acelerator. This download accelerator could be provided as part of onionshare. For more details, see https://github.com/micahflee/onionshare/issues/1295.


8 Vanguards doc updates, bugfixes, packages for onionshare and/or Tor Browser
- Pad: https://pad.riseup.net/p/2021-hackweek-vanguards-docs
- Contacts: Mike (UTC-6)
- Skills: Python, tech doc writing and/or review, packaging/build eng
- Summary: The vanguards Tor control port addon provides defense against Guard discovery attacks, as well as other attacks against onion services, onion clients, and even Tor Browser exit traffic. Vanguards is in need of some doc updates, bugfixes, and we could even package it for one of onionshare or Tor Browser. Packaging it with Onionshare also helps improve the security properties of the above download accelerator item. See the bugtracker for specific items: https://github.com/mikeperry-tor/vanguards/issues


9 Circuit Padding Simulator
- Pad: https://pad.riseup.net/p/2021-hackweek-circuit-padding
- Contacts: Mike (UTC-6). Or maybe Tobias Pulls (UTC+1), or maybe asn (UTC+1) 
- Skills: Tor unit test framework, Tor C, maybe python
- Summary: The Tor circuit padding framework is under active use by researchers to improve padding defenses against website traffic fingerprinting, and onion service circuit fingerprinting. The simulator is in need of update to the latest Tor release, as well as in need of performance improvements, and a more accurate way to deal with time-based features. See https://github.com/pylls/circpad-sim/blob/master/README.md


## How to add a new project

There are several ways you can propose a project:
- create a merge request against this repository
- add a new project in [the public pad with projects](http://pad.riseup.net/p/tor-hackweek-2021-March-keep). Somebody will add them to the repo later.

## How to add yourself to a team

Each project has a pad linked. Write your name under "Team"'s section.

## Questions?

- add any question you may have to the pad or send a mail to gaba at torproject dot org.
